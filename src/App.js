import React from 'react';
import './App.css';
import Bar from './Bar/Bar'
import RegisterForm from './Form/RegisterForm'
import Jumbotron from './Jumbo/Jumbotron';

function App() {
  return (
    <div className="App">
      <Bar />
      <Jumbotron />
      <RegisterForm />
    </div>
  );
}

export default App;
