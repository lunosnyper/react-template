import React from 'react';
import { render, fireEvent, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import axiosMock from 'axios'
import RegisterForm from './RegisterForm'

jest.mock('axios')

test('has a form', () => {
    const { getByText } = render(<RegisterForm />);

    expect(getByText('Submit')).toBeInTheDocument();
})

test('has a title', () => {
    const { getByText } = render(<RegisterForm />);

    expect(getByText('Register')).toBeInTheDocument();
})

test('has a firstname input', () => {
    const { getByLabelText } = render(<RegisterForm />);

    const firstNameInput = getByLabelText('First Name')
    fireEvent.change(firstNameInput, {target:{value:"first name"}})
    expect(firstNameInput.value).toBe("first name")
})

test('has a lastname input', () => {
    const { getByLabelText } = render(<RegisterForm />);

    expect(getByLabelText('Last Name')).toBeInTheDocument();
})

test('has a email input', () => {
    const { getByLabelText } = render(<RegisterForm />);

    expect(getByLabelText('Email')).toBeInTheDocument();
})

test('can send a form post', async () => {
    const { getByText, getByRole } = render(<RegisterForm />)

    axiosMock.post.mockResolvedValueOnce({data: {}})

    fireEvent.click(getByRole('button'))

    await wait(() => expect(getByText("Success")).toBeVisible())
    await wait(() => expect(axiosMock.post).toHaveBeenCalledTimes(1))
})