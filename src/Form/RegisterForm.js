import React, { useState } from 'react';
import { Button, TextField, Grid, Card, CardContent, CardActions } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography';
import Axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
        display: 'block',
        flexWrap: 'wrap',
        marginTop: theme.spacing(2)
    },
    textField: {
        margin: theme.spacing(1),
        width: 300
    },
    button: {
        marginLeft: theme.spacing(1),
        marginBottom: theme.spacing(1)
    }
}));

const host = process.env.NODE_ENV === 'production' ? 'https://lottry-rest-8080' : 'http://localhost:8080';
const hostUri = host + "/people"

export default function RegisterForm() {
    const classes = useStyles();
    const [posted, setPosted] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = new FormData(event.target);
        await Axios.post(
            hostUri,
            {
                "firstName": data.get("firstName"),
                "lastName": data.get("lastName"),
                "email": data.get("email")
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        setPosted('Success')
    }

    return (
        <Container maxWidth="sm">
            <Card className={classes.root} variant="outlined">
                <form onSubmit={handleSubmit}>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Register
                        </Typography>
                        <Grid >
                            <div>
                                <Grid item xs={12} >
                                    <TextField id="first-name" label="First Name" name="firstName" fullWidth />
                                </Grid>
                                <Grid item xs={12} >
                                    <TextField id="last-name" label="Last Name" name="lastName" fullWidth />
                                </Grid>
                                <Grid item xs={12} >
                                    <TextField id="email" label="Email" name="email" fullWidth />
                                </Grid>
                            </div>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Button type="submit" variant="contained" className={classes.button}>
                            Submit
                        </Button>
                        <p>{posted}</p>
                    </CardActions>
                </form>
            </Card>
        </Container>
    )
}