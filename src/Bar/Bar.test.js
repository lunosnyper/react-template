import React from 'react'
import { render, fireEvent} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Bar from './Bar'

test('App bar loads with title', () => {
    // given
    const { getByText } = render(<Bar />)

    expect(getByText('React Template')).toBeInTheDocument()
})