import React from 'react';
import { render, fireEvent, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import Jumbotron from './Jumbotron';

test('find a title', () => {
    const {getByRole} = render(<Jumbotron />);

    expect(getByRole('heading').innerHTML).toContain('React Template')
})