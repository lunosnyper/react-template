import React from 'react'
import {Grid, makeStyles, Container, Box} from '@material-ui/core'
import './Jumbotron.css'

const useStyles = makeStyles(theme => ({
    root: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        padding: '0 30px',
    },
    paper: {
        background: 'transparent',
        padding: theme.spacing(2),
        textAlign: 'center',
    }
}));

export default function Jumbotron() {

    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <Grid container item xs={12}>
                <Container>
                    <Box className={classes.paper}>
                        <h1>Welcome to React Template</h1>
                    </Box>
                </Container>
            </Grid>
        </Grid>
    );
} 