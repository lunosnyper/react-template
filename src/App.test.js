import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders title', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText('React Template');
  expect(linkElement).toBeInTheDocument();
});

test('renders form', () => {
  const { getByText } = render(<App />);
  const formElement = getByText('Submit');
  expect(formElement).toBeInTheDocument();
});
