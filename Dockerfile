FROM node:13
COPY build .
EXPOSE 5000
RUN ["npm", "install", "-g", "serve"]
CMD ["serve", "-s", "."]